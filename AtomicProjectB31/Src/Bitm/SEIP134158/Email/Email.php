<?php

namespace App\Bitm\SEIP134158\Email;
use PDO;
use App\Bitm\SEIP134158\Utility\Utility;
use App\Bitm\SEIP134158\Message\Message;


class Email
{
    public $id='';
    public $name='';
    public $email='';
    public $mobile='';
    public $serverName='localhost';
    public $dbname='atomicproject';
    public $user='root';
    public $pass='';
    public $conn;

    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->dbname", $this->user, $this->pass);
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function setData($d=''){
        if(array_key_exists('name',$d) && !empty($d)){
            $this->title=$d['name'];
        }
        if(array_key_exists('email',$d) && !empty($d)){
            $this->title=$d['email'];
        }
        if(array_key_exists('mobile',$d) && !empty($d)){
            $this->title=$d['mobile'];
        }
        return $this;
    }

    public function store(){
        $myq = "INSERT INTO `email`(`name`,`email`,`mobile`) VALUES (:name,:email,:mobile)";
        $statement=$this->conn->prepare($myq);
        $result = $statement->execute(array(':name'=>$this->name, ':email'=>$this->email,':mobile'=>$this->mobile));
    }

}